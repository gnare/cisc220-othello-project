/*
Galen Nare 9/15/2020

Intended for use with CISC220 Othello Mini Project ONLY. Do not distribute.
*/

#include <stdlib.h>
#include <time.h>

#include "othello.hpp"

using namespace std;

int main(void) {
    cout << "===== OTHELLO =====" << endl;
    srand(time(NULL));
    string p1name = "", p2name = "";
    cout << "Enter name for Player 1 (Enter \"CPU\" for computer player): ";
    cin >> p1name;
    cout << "Enter name for Player 2 (Enter \"CPU\" for computer player): ";
    cin >> p2name;

    if (p1name == "CPU" && p2name == "CPU") {
        Othello game;
        game.play_game();
    } else if (p1name != "CPU" && p2name == "CPU") {
        Othello game(p1name, 'B');
        game.play_game();
    } else if (p1name == "CPU" && p2name != "CPU") {
        Othello game(p2name, 'B');
        game.play_game();
    } else {
        Othello game(p1name, 'B', p2name, 'O');
        game.play_game();
    }
}
