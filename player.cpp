/*
Galen Nare 9/15/2020

Intended for use with CISC220 Othello Mini Project ONLY. Do not distribute.
*/

#include "player.hpp"

Player::Player() {
    Player::name = "Computer";
    isCpu = true;
    Player::color = 'O';
}

Player::Player(std::string name, char color) {
    Player::name = name;
    isCpu = false;
    Player::color = color;
}