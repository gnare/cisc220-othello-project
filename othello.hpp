/*
Galen Nare 9/15/2020

Intended for use with CISC220 Othello Mini Project ONLY. Do not distribute.
*/

#include "player.hpp"

// Computer player options
#define CPU_FORCE_MOVE true             // This will force the CPU to move if they have a valid move. The game is trivially easy if set to false.
#define CPU_SELECT_BEST_MOVE false      // This will force the CPU to pick the best possible move (maximum number of pieces flipped). Will make the game much harder.

using namespace std;

#ifndef Othello
    class Othello {
        char* board;
        const int boardSize = 8;
        int numPlayers;
        Player player1;
        Player player2;

        public:
            // Constructors
            Othello();
            Othello(string, char);
            Othello(string, char, string, char);

            // Public methods
            void make_mat(char[]);
            void print_mat();
            bool place_piece(Player player);
            int count_and_flip_pieces(int x, int y, Player p);
            int count_and_flip_pieces(int x, int y, Player p, bool doFlip);
            int board_idx(int x, int y);
            bool comp_place_piece(Player p);
            void ck_win();
            void play_game();
    };
#endif