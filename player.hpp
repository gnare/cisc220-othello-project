/*
Galen Nare 9/15/2020

Intended for use with CISC220 Othello Mini Project ONLY. Do not distribute.
*/

#include <iostream>

#ifndef Player
    class Player {
        public:
            // Public fields
            std::string name;
            char color;
            bool isCpu;

            // Constructors
            Player();
            Player(std::string, char);
            
    };
#endif