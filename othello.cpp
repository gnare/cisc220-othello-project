/*
Galen Nare 9/15/2020

Intended for use with CISC220 Othello Mini Project ONLY. Do not distribute.
*/

#include <vector>
#include "othello.hpp"

Othello::Othello() {
    Othello::player1 = Player();
    Othello::player2 = Player();
    Othello::player2.name = "Computer 2";
    Othello::player2.color = (player1.color == 'B') ? 'O' : 'B'; // Computer color defaults to O, so pick the other one from P1
    Othello::numPlayers = 0;
    Othello::board = new char[boardSize * boardSize];
    Othello::make_mat(Othello::board);
}

Othello::Othello(string str1, char c1) {
    Othello::player1 = Player(str1, c1);
    Othello::player2 = Player();
    Othello::player2.color = (player1.color == 'B') ? 'O' : 'B'; // Computer color defaults to O, so pick the other one from P1
    Othello::numPlayers = 1;
    Othello::board = new char[boardSize * boardSize];
    Othello::make_mat(Othello::board);
}

Othello::Othello(string str1, char c1, string str2, char c2) {
    Othello::player1 = Player(str1, c1);
    Othello::player2 = Player(str2, c2);
    Othello::numPlayers = 2;
    Othello::board = new char[boardSize * boardSize];
    Othello::make_mat(Othello::board);
}

void Othello::make_mat(char mat[]) {
    for (int i = 0; i < boardSize * boardSize; i++) { 
        mat[i] = '-'; // Fill the board with - (empty spaces)
    }

    mat[(boardSize / 2) * boardSize + (boardSize / 2)] = 'O'; // Set up the peices on the center of the board
    mat[(boardSize / 2 - 1) * boardSize + (boardSize / 2)] = 'B';
    mat[(boardSize / 2 - 1) * boardSize + (boardSize / 2 - 1)] = 'O';
    mat[(boardSize / 2) * boardSize + (boardSize / 2 - 1)] = 'B';
}

void Othello::print_mat() {
    for (int x = 0; x < boardSize; x++) {
        cout << "\t" << x;
    }
    cout << endl;
    for (int i = 0; i < boardSize * boardSize; i++) {
        if (i % boardSize == 0) {
            if (i != 0) {
                cout << endl;
                cout << endl;
            }
            cout << i / boardSize << "\t";
        }
        cout << board[i] << "\t";
    }
    cout << endl;
}

int Othello::board_idx(int x, int y) {
    return y * boardSize + x;
}

void Othello::ck_win() {
    int p1count = 0, p2count = 0;

    for (int i = 0; i < boardSize * boardSize; i++) {
        if (board[i] == player1.color) {
            p1count++;
        } else if (board[i] == player2.color) {
            p2count++;
        }
    }

    if (p1count > p2count) {
        cout << player1.name << " wins! (" << p1count << "-" << p2count << ")" << endl;
    } else if (p2count > p1count) {
        cout << player2.name << " wins! (" << p2count << "-" << p1count << ")" << endl;
    } else {
        cout << "Draw. (" << p1count << "-" << p2count << ")" << endl;
    }
}

int Othello::count_and_flip_pieces(int x, int y, Player p, bool doFlip) {
    // check 8 directions
    const int startX = x, startY = y;
    const int xmodlist[] = {-1, 1, 0, 0, 1, -1, 1, -1};
    const int ymodlist[] = {0, 0, 1, -1, 1, 1, -1, -1};
    vector<int> nextFlipIdx;
    int flipped = 0;

    for (int i = 0; i < 8; i++) {
        int xmod = xmodlist[i];
        int ymod = ymodlist[i];
        x = startX;
        y = startY;
        while (x >= 0 && x < boardSize && y >= 0 && y < boardSize) {
            int cx = (xmod < 0) ? boardSize - 1 : ((xmod == 0) ? startX : 0);
            int cy = (ymod < 0) ? boardSize - 1 : ((ymod == 0) ? startY : 0);
            int idx = board_idx(x, y);
            if (x == startX && y == startY) { // Don't count piece being placed
                x += xmod; // Move to next location
                y += ymod;
                continue;
            }
            if (board[idx] != p.color && board[idx] != '-') { // Determine flippable pieces
                nextFlipIdx.push_back(idx);
            } else if (board[idx] == p.color) { // Flip pieces
                for (int j = 0; j < nextFlipIdx.size(); j++) {
                    if (doFlip) {
                        board[nextFlipIdx.at(j)] = p.color;
                    }
                    flipped++;
                }
                break;
            } else { // Empty space, try different direction
                break;
            }
            x += xmod; // Move to next location
            y += ymod;
        }
    }

    return flipped;
}

int Othello::count_and_flip_pieces(int x, int y, Player p) {
    return count_and_flip_pieces(x, y, p, true);
}

bool Othello::place_piece(Player player) {
    bool isValid = false;
    while (!isValid) {
        int x, y;
        cout << "Enter the X coord you would like to place your piece: ";
        cin >> x; 
        cout<< "Enter the Y coord you would like to place your piece: ";
        cin >> y;

        if (x >= 0 && x < boardSize && y >= 0 && y < boardSize && board[board_idx(x, y)] == '-') {
            isValid = true;
            int flipped = count_and_flip_pieces(x, y, player, false);
            if (flipped > 0) {
                count_and_flip_pieces(x, y, player, true);
                cout << player.name << " flipped " << flipped << " pieces." << endl;
                board[board_idx(x, y)] = player.color;
                return true;
            } else {
                cout << player.name << " forefeited their turn." << endl;
                return false;
            }
        } else {
            cout << "Input valid coordinates. The selected space must be empty." << endl;
        }
    }
}

bool Othello::comp_place_piece(Player p) {
    // Create lists of maximum value coords.
    vector<int> max, maxX, maxY;
    for (int i = 0; i < boardSize * boardSize; i++) {
        int y = i / boardSize;
        int x = i % boardSize;
        if (board[i] != '-') {
            continue; // Don't try to replace filled spaces
        }
        int flipped = count_and_flip_pieces(x, y, p, false);
        if (max.size() == 0 || flipped > max.begin()[0]) {
            max.insert(max.begin(), flipped);
            maxX.insert(maxX.begin(), x);
            maxY.insert(maxY.begin(), y);
        }
    }

    // Select a move from the top 4 moves based on CPU options
    vector<int>::iterator maxIter = max.begin();
    if (maxIter[0] > 0) {
        int sel_move;
        if (!CPU_SELECT_BEST_MOVE) {
            sel_move = rand() % 4;
            while (maxIter[sel_move] == 0 && CPU_FORCE_MOVE) {
                sel_move = rand() % 4;
            }
        } else {
            sel_move = 0;
        }

        // Execute move
        int count = count_and_flip_pieces(maxX.begin()[sel_move], maxY.begin()[sel_move], p);
        if (count > 0) {
            cout << p.name << " flipped " << count << " pieces." << endl;
            board[board_idx(maxX.begin()[sel_move], maxY.begin()[sel_move])] = p.color;
            return true;
        } else {
            cout << p.name << " forefeits their turn." << endl;
            return false;
        }
    } else {
        // No valid move
        cout << p.name << " forefeits their turn." << endl;
        return false;
    }
}

void Othello::play_game() {
    // Setup
    int filledSpaces = 0;
    bool p1canPlay = true, p2CanPlay = true;
    bool playerTurn = rand() % 2 == 1;

    // Game loop
    cout << "STARTING BOARD" << endl;
    print_mat();
    while (filledSpaces < boardSize * boardSize && (p1canPlay || p2CanPlay)) {
        Player p = (playerTurn) ? player2 : player1;
        if (p.isCpu) {
            cout << p.name << " is playing their turn." << endl;
            bool result = comp_place_piece(p);
            if (playerTurn) {
                p1canPlay = result;
            } else {
                p2CanPlay = result;
            }
        } else {
            cout << p.name << "\'s (" << p.color << ") turn." << endl;
            bool result = place_piece(p);
            if (playerTurn) {
                p1canPlay = result;
            } else {
                p2CanPlay = result;
            }
        }
        playerTurn = !playerTurn;

        print_mat();
    }

    // End game
    if (!p1canPlay && !p2CanPlay) {
        cout << "Neither player has a valid move." << endl;
    }

    cout << "====== GAME OVER ======" << endl;
    ck_win();
}